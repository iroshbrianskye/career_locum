from datetime import datetime

from .. import db


class LocumAcademicDetails(db.Model):
    __tablename__ = 'locum_academic_details'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    year_from = db.Column(db.String(64))
    year_to = db.Column(db.String(64))
    name_of_institution = db.Column(db.String(2000))
    course = db.Column(db.String(2000))
    award = db.Column(db.String(2000))
    specialization = db.Column(db.String(2000))
    class_grade = db.Column(db.String(2000))

    def __repr__(self):
        return '<LocumAcademicDetails\'%s\'>' % self.course

