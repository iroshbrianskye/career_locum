from datetime import datetime

from .. import db


class Professional(db.Model):
    __tablename__ = 'professional'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    year_from = db.Column(db.String(256))
    year_to = db.Column(db.String(256))
    name_of_institution = db.Column(db.String(256))
    award = db.Column(db.String(256))
    specialization = db.Column(db.String(256))
    class_grade = db.Column(db.String(256))

    def __repr__(self):
        return '<Professional\'%s\'>' % self.id
