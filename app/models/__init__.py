"""
These imports enable us to make all defined models members of the models
module (as opposed to just their python files)
"""

from .users import *
from .category import *
from .job_listings import *
from .applications import *
from .education import *
from .images import *
from .messages import *
from .education_documents import *
from .roles import *
from .notifications import *
from .publisher import *
from .applicant import *
from .employment_history import *
from .disclaimer import *
from .family import *
from .reference import *
from .professional import *
from .memberships import *
from .manual_entry import *
from .certificate import *
from .degree import *
from .diploma import *
from .higher_diploma import *
from .masters import *
from .phd import *
from .days_available import *
from .locum_academic_details import *
from .locum_area_of_professional_interest import *
from .locum_availability import *
from .locum_employment import *
from .locum_other_personal import *
from .locum_relevant_course import *
from .jazz import *
from .license import *
