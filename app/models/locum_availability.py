from datetime import datetime

from .. import db


class LocumAvailability(db.Model):
    __tablename__ = 'locum_availability'
    id = db.Column(db.Integer, primary_key=True)
    days_per_month = db.Column(db.Integer())
    soonest_date_available = db.Column(db.String(200))
    affecting_conditions = db.Column(db.String(200))
    reasons = db.Column(db.String(2000))
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id'), index=True)
    timestamp = db.Column(db.DateTime(), default=datetime.utcnow)
    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<LocumAvailability \'%s\'>' % self.id
