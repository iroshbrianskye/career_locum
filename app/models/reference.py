from datetime import datetime

from .. import db


class Reference(db.Model):
    __tablename__ = 'reference'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    address = db.Column(db.String(64))
    name = db.Column(db.String(64), index=True)
    phone_number = db.Column(db.String(64), index=True)
    email_address = db.Column(db.String(64), index=True)
    occupation = db.Column(db.String(64), index=True)
    institution = db.Column(db.String(64), index=True)
    address_2 = db.Column(db.String(64))
    name_2 = db.Column(db.String(64), index=True)
    phone_number_2 = db.Column(db.String(64), index=True)
    email_address_2 = db.Column(db.String(64), index=True)
    occupation_2 = db.Column(db.String(64), index=True)
    institution_2 = db.Column(db.String(64), index=True)

    timestamp = db.Column(db.DateTime(), default=datetime.utcnow)
    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Reference\'%s\'>' % self.name


