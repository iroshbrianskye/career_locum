from flask import current_app, url_for, request, session, redirect
from flask_login import AnonymousUserMixin, UserMixin
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from itsdangerous import BadSignature, SignatureExpired
from werkzeug.security import check_password_hash, generate_password_hash
import json
from .. import db, login_manager
from .roles import Role
from app.models.messages import *
from app.models.applications import *
from app.models.job_listings import *
from app.models.notifications import *
from app.models.publisher import *
import jwt
from time import time
import base64
from datetime import datetime, timedelta
from hashlib import md5
import os
import urllib3

http = urllib3.PoolManager()


class Permission:
    USER = 0x01
    APPLICANT = 0x07
    PUBLISHER = 0x08
    CAN_LIKE = 0x01
    CAN_POST = 0x02
    CAN_EDIT = 0x04
    CAN_REMOVE = 0x08
    ADMINISTRATOR = 0xff


class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    confirmed = db.Column(db.Boolean, default=False)
    first_name = db.Column(db.String(64), index=True)
    surname = db.Column(db.String(64), index=True)
    second_name = db.Column(db.String(100))
    gender = db.Column(db.String(64))
    id_number = db.Column(db.String(100))
    photo = db.Column(db.String(100))
    email = db.Column(db.String(64), index=True)
    phone_number_personal = db.Column(db.String(100), index=True)
    password_hash = db.Column(db.String(128))
    postal_address = db.Column(db.String(100))
    town = db.Column(db.String(100))
    dob = db.Column(db.String(100))
    home_county = db.Column(db.String(100))
    country_of_residence = db.Column(db.String(256))
    area_of_residence = db.Column(db.String(256))
    ethnicity = db.Column(db.String(64))
    phone_number_alternate = db.Column(db.String(100))
    alternative_contact_person = db.Column(db.String(100))
    alternate_contact_person_telephone = db.Column(db.String(100))
    title = db.Column(db.String(64), index=True)
    civil_status = db.Column(db.String(64))
    nationality = db.Column(db.String(100))
    disability = db.Column(db.Boolean, default=False)
    disability_nature = db.Column(db.String(120))
    disability_registration_details = db.Column(db.String(120))
    active = db.Column(db.Boolean, default=True)
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))
    can_apply = db.Column(db.Boolean, default=False)
    last_seen = db.Column(db.DateTime, default=datetime.utcnow)
    last_message_read_time = db.Column(db.DateTime)
    last_application_read_time = db.Column(db.DateTime)
    token = db.Column(db.String(32), index=True, unique=True)
    token_expiration = db.Column(db.DateTime)
    county = db.Column(db.String(100))
    highest_level_education = db.Column(db.String(64))
    exact_degree_diploma_title = db.Column(db.String(100))

    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    # Relationships
    locum_other_personal = db.relationship('LocumOtherPersonal', backref='user', lazy='dynamic')
    locum_availability = db.relationship('LocumAvailability', backref='user', lazy='dynamic')
    locum_employment = db.relationship('LocumEmployment', backref='user', lazy='dynamic')
    locum_employment_history = db.relationship('LocumEmploymentHistory', backref='user', lazy='dynamic')
    locum_academic_details = db.relationship('LocumAcademicDetails', backref='user', lazy='dynamic')
    locum_relevant_course = db.relationship('LocumRelevantCourse', backref='user', lazy='dynamic')
    locum_area_of_professional_interest = db.relationship('LocumAreaOfProfessionalInterest', backref='user', lazy='dynamic')

    education = db.relationship('Education', backref='user', lazy='dynamic')
    phd = db.relationship('Phd', backref='user', lazy='dynamic')
    masters = db.relationship('Masters', backref='user', lazy='dynamic')
    degree = db.relationship('Degree', backref='user', lazy='dynamic')
    higher_diploma = db.relationship('HigherDiploma', backref='user', lazy='dynamic')
    diploma = db.relationship('Diploma', backref='user', lazy='dynamic')
    certificate = db.relationship('Certificate', backref='user', lazy='dynamic')
    education_documents = db.relationship('EducationDocuments', backref='user', lazy='dynamic')
    professional = db.relationship('Professional', backref='user', lazy='dynamic')
    membership = db.relationship('Membership', backref='user', lazy='dynamic')
    employment = db.relationship('Employment', backref='user', lazy='dynamic')
    employment_history = db.relationship('EmploymentHistory', backref='user', lazy='dynamic')
    family_info = db.relationship('Family', backref='user', lazy='dynamic')
    references = db.relationship('Reference', backref='user', lazy='dynamic')
    applicants = db.relationship('Applicant', backref='user', lazy='dynamic')
    applications = db.relationship('Application', backref='user', lazy='dynamic')
    publisher = db.relationship('Publisher', backref='user', uselist=False, cascade='all,delete-orphan')
    job_listings = db.relationship('Listing', backref='publisher', lazy='dynamic')
    manual_entries = db.relationship('ManualEntry', backref='user', lazy='dynamic')
    messages_sent = db.relationship('Message',
                                    foreign_keys='Message.sender_id',
                                    backref='author', lazy='dynamic')
    messages_received = db.relationship('Message',
                                        foreign_keys='Message.recipient_id',
                                        backref='recipient', lazy='dynamic')
    notifications = db.relationship('Notification', backref='user',
                                    lazy='dynamic')

    def __init__(self, **kwargs):
        super(User, self).__init__(**kwargs)
        if self.role is None:
            if self.email == current_app.config['ADMIN_EMAIL']:
                self.role = Role.query.filter_by(
                    permissions=Permission.ADMINISTRATOR).first()
            if self.role is None:
                self.role = Role.query.filter_by(default=True).first()

    def full_name(self):
        return '%s %s' % (self.first_name, self.surname)

    def avatar(self, size):
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()
        return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(
            digest, size)

    def can(self, permissions):
        return self.role is not None and \
               (self.role.permissions & permissions) == permissions

    def can_submit(self):
        return self.first_name is not None and self.surname is not None and \
               self.second_name is not None and self.gender is not None and \
               self.home_county is not None and self.dob is not None and self.civil_status is not None and \
               self.disability is not None and self.email is not None and \
               self.phone_number_personal is not None and self.locum_availability is not None\
               and self.locum_academic_details is not None and \
               self.locum_employment is not None and self.locum_other_personal is not None \
               and self.locum_area_of_professional_interest is not None


    def is_admin(self):
        return self.can(Permission.ADMINISTRATOR)

    @property
    def password(self):
        raise AttributeError('`password` is not a readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)

    def generate_confirmation_token(self, expiration=604800):
        """Generate a confirmation token to email a new user."""

        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        self.token = self.id
        self.token_expiration = expiration
        db.session.add(self)
        db.session.commit()
        return s.dumps({'confirm': self.id})

    def generate_email_change_token(self, new_email, expiration=3600):
        """Generate an email change token to email an existing user."""
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'change_email': self.id, 'new_email': new_email})

    def generate_password_reset_token(self, expiration=3600):
        """
        Generate a password reset change token to email to an existing user.
        """
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'reset': self.id})

    def get_reset_password_token(self, expires_in=600):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            current_app.config['SECRET_KEY'],
            algorithm='HS256').decode('utf-8')

    def confirm_account(self, token):
        """Verify that the provided token is for this user's id."""
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except (BadSignature, SignatureExpired):
            return False
        if data.get('confirm') != self.id:
            return False
        self.confirmed = True
        db.session.add(self)
        db.session.commit()
        return True

    def get_confirmation_token(self, expires_in=600):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            current_app.config['SECRET_KEY'],
            algorithm='HS256').decode('utf-8')

    @staticmethod
    def verify_confirmation_token(token):
        try:
            id = jwt.decode(token, current_app.config['SECRET_KEY'],
                            algorithms=['HS256'])['reset_password']
        except:
            return
        return User.query.get(id)

    def change_email(self, token):
        """Verify the new email for this user."""
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except (BadSignature, SignatureExpired):
            return False
        if data.get('change_email') != self.id:
            return False
        new_email = data.get('new_email')
        if new_email is None:
            return False
        if self.query.filter_by(email=new_email).first() is not None:
            return False
        self.email = new_email
        db.session.add(self)
        db.session.commit()
        return True

    def reset_password(self, token, new_password):
        """Verify the new password for this user."""
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except (BadSignature, SignatureExpired):
            return False
        if data.get('reset') != self.id:
            return False
        self.password = new_password
        db.session.add(self)
        db.session.commit()
        return True

    def get_token(self, expires_in=3600):
        now = datetime.utcnow()
        if self.token and self.token_expiration > now + timedelta(seconds=60):
            return self.token
        self.token = base64.b64encode(os.urandom(24)).decode('utf-8')
        self.token_expiration = now + timedelta(seconds=expires_in)
        db.session.add(self)
        db.session.commit()
        return self.token

    def revoke_token(self):
        self.token_expiration = datetime.utcnow() - timedelta(seconds=1)

    @staticmethod
    def check_token(token):
        user = User.query.filter_by(token=token).first()
        if user is None:
            return None
        return user

    @staticmethod
    def generate_fake(count=100, **kwargs):
        """Generate a number of fake users for testing."""
        from sqlalchemy.exc import IntegrityError
        from random import seed, choice
        from faker import Faker

        fake = Faker()
        roles = Role.query.all()

        seed()
        for i in range(count):
            u = User(
                first_name=fake.first_name(),
                second_name=fake.last_name(),
                email=fake.email(),
                surname='',
                photo='',
                password='password',
                confirmed=True,
                active=True,
                role=choice(roles),
                gender=fake.gender(),
                id_number='',
                phone_number_personal=fake.phone_number(),
                address=fake.address(),
                dob=fake.date_of_birth(),
                pob='Kenya',
                phone_number_alternate=fake.phone_number(),
                title='',
                civil_status='',
                nationality='Kenyan',
                can_apply=True,
                **kwargs)
            db.session.add(u)
            try:
                db.session.commit()
            except IntegrityError:
                db.session.rollback()

    def __repr__(self):
        return '<User \'%s\'>' % self.full_name()

    def new_messages(self):
        last_read_time = self.last_message_read_time or datetime(1900, 1, 1)
        return Message.query.filter_by(recipient=self).filter(
            Message.timestamp > last_read_time).count()

    def new_applications(self):
        last_read_time = self.last_application_read_time or datetime(1900, 1, 1)
        list = [(row.id) for row in self.listings]
        applications = 0
        if list:
            applications = Application.query.filter(Application.job_listing_id.in_(list)).filter(
                Application.createdAt > last_read_time).count()
        return applications

    def add_notification(self, name, data):
        self.notifications.filter_by(name=name).delete()
        n = Notification(name=name, payload_json=json.dumps(data), user=self)
        db.session.add(n)
        return n

    def get_job_count(self):
        return self.job_listings.group_by(Listing.job_reference).count()

    def user_messages(self, recipient):
        sent = Message.query.filter_by(author=self, recipient=recipient)
        own = Message.query.filter_by(recipient=self, author=recipient)
        return sent.union(own).order_by(Message.timestamp.asc())

    def applications_count(self):
        applications = Application.query.join(
            Listing, (Listing.id == Application.job_listing_id)).filter(
            Listing.publisher_id == self.id)
        return Application.count()

    def from_dict(self, data, new_user=False):
        for field in ['username', 'email', 'about_me']:
            if field in data:
                setattr(self, field, data[field])
        if new_user and 'password' in data:
            self.set_password(data['password'])

    def to_dict(self, include_email=False):
        data = {
            'id': self.id,
            'username': self.username,
            'role': self.role,

        }
        if include_email:
            data['email'] = self.email
        return data

    @staticmethod
    def get_class_by_tablename(tablename):
        """Return class reference mapped to table.

        :param tablename: String with name of table.
        :return: Class reference or None.
        """
        for c in db.Model._decl_class_registry.values():
            if hasattr(c, '__tablename__') and c.__tablename__ == tablename:
                return c


class AnonymousUser(AnonymousUserMixin):
    def can(self, _):
        return False

    def is_admin(self):
        return False


login_manager.anonymous_user = AnonymousUser


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))
