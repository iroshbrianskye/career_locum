from datetime import datetime

from .. import db
from flask_moment import Moment


class DaysAvailable(db.Model):
    __tablename__ = 'days_available'
    id = db.Column(db.Integer, primary_key=True)
    application_id = db.Column(db.Integer, db.ForeignKey('applications.id'))
    monday = db.Column(db.Boolean(), index=True, default=False)
    tuesday = db.Column(db.Boolean(), index=True, default=False)
    wednesday = db.Column(db.Boolean(), index=True, default=False)
    thursday = db.Column(db.Boolean(), index=True, default=False)
    friday = db.Column(db.Boolean(), index=True, default=False)
    saturday = db.Column(db.Boolean(), index=True, default=False)
    sunday = db.Column(db.Boolean(), index=True, default=False)
    timestamp = db.Column(db.DateTime(), default=datetime.utcnow)
    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<DaysAvailable\'%s\'>' % self.application_id
