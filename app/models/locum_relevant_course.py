from datetime import datetime

from .. import db


class LocumRelevantCourse(db.Model):
    __tablename__ = 'locum_relevant_course'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    year_from = db.Column(db.String(64))
    year_to = db.Column(db.String(64))
    name_of_institution = db.Column(db.String(256))
    name_of_course = db.Column(db.String(256))
    details = db.Column(db.String(2000))

    def __repr__(self):
        return '<LocumRelevantCourse\'%s\'>' % self.name_of_course

