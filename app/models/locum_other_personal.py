from datetime import datetime

from .. import db


class LocumOtherPersonal(db.Model):
    __tablename__ = 'locum_other_personal'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    if_ever_convicted = db.Column(db.String(64))
    nature_of_offence = db.Column(db.String(2000))
    year_of_conviction = db.Column(db.DateTime())
    if_ever_dismissed = db.Column(db.String(64))
    reason_for_dismissal = db.Column(db.String(2000))
    effective_date = db.Column(db.String(2000))

    timestamp = db.Column(db.DateTime(), default=datetime.utcnow)
    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<LocumOtherPersonal\'%s\'>' % self.id


