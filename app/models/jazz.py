from datetime import datetime

from .. import db


class Jazz(db.Model):
    __tablename__ = 'jazz'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    cv_url = db.Column(db.String(256))
    application_id = db.Column(db.Integer, db.ForeignKey('applications.id'))
    timestamp = db.Column(db.DateTime(), default=datetime.utcnow)
    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Jazz\'%s\'>' % self.id


