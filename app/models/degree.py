from datetime import datetime

from .. import db


class Degree(db.Model):
    __tablename__ = 'degree_information'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    year_ended = db.Column(db.String(64), index=True)
    name_of_institution = db.Column(db.String(64), index=True)
    certificate_obtained = db.Column(db.String(64), index=True)
    timestamp = db.Column(db.DateTime(), default=datetime.utcnow)
    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Degree\'%s\'>' % self.id

