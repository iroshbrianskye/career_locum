from datetime import datetime

from .. import db


class LocumAreaOfProfessionalInterest(db.Model):
    __tablename__ = 'locum_area_of_professional_interest'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))

    name_of_interest = db.Column(db.String(2000), index=True)

    def __repr__(self):
        return '<LocumAreaOfProfessionalInterest\'%s\'>' % self.name_of_interest

