from datetime import datetime

from .. import db


class LocumEmploymentHistory(db.Model):
    __tablename__ = 'locum_employment_history'
    id = db.Column(db.Integer, primary_key=True)
    name_of_institution = db.Column(db.String(64), index=True)
    year_from = db.Column(db.String(64), index=True)
    year_to = db.Column(db.String(64))
    duration = db.Column(db.String(64), index=True)
    job_group = db.Column(db.String(64), index=True)
    designation = db.Column(db.String(64))
    gross_salary = db.Column(db.String(64))
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id'), index=True)
    timestamp = db.Column(db.DateTime(), default=datetime.utcnow)
    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<LocumEmploymentHistory \'%s\'>' % self.id


class LocumEmployment(db.Model):
    __tablename__ = 'locum_employment'
    id = db.Column(db.Integer, primary_key=True)
    if_not_employed = db.Column(db.Boolean, default=False)
    full_time_employed = db.Column(db.Boolean, default=False)
    full_time_employer = db.Column(db.String(256), index=True)
    full_time_county = db.Column(db.String(256), index=True)
    locum_employed = db.Column(db.Boolean, default=False)
    locum_employer = db.Column(db.String(256), index=True)
    locum_county = db.Column(db.String(256), index=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('users.id'), index=True)
    terms_of_service = db.Column(db.String(256), index=True)
    position_held = db.Column(db.String(256))

    def __repr__(self):
        return '<LocumEmployment \'%s\'>' % self.id
