from datetime import datetime

from .. import db


class Education(db.Model):
    __tablename__ = 'education'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
    year_ended = db.Column(db.String(64), index=True)
    name_of_institution = db.Column(db.String(2000), index=True)
    certificate_obtained = db.Column(db.String(2000), index=True)
    timestamp = db.Column(db.DateTime(), default=datetime.utcnow)
    createdAt = db.Column(db.DateTime(), default=datetime.utcnow)
    updatedAt = db.Column(db.DateTime(), default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return '<Education\'%s\'>' % self.id

