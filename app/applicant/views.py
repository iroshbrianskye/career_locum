import uuid

from flask import (
    Blueprint,
    render_template,
    redirect,
    url_for,
    jsonify,
    flash,
    send_from_directory)
from flask_ckeditor import upload_success, upload_fail
from flask_login import current_user, login_required
import PIL, simplejson, traceback
from PIL import Image
from flask_rq import get_queue
from flask_uploads import patch_request_class

from app.models.job_listings import *
from app.models.applications import *
from app.models.education_documents import *
from app.models.users import *
from app.applicant.forms import *
from app.auth.forms import *
from app import db
from app.email import send_email
from app.models import *
from app.auth.email import send_password_reset_email
from app.auth.admin_decorators import check_confirmed
from app.decorators import checks_required
from werkzeug.utils import secure_filename
from app.lib.upload_file import uploadfile
from app.auth.email import send_applicant_submitted
from werkzeug.datastructures import FileStorage, MultiDict
import africastalking

username = "kutrrh"
api_key = "316dda421e1e2ee09e95d218ee5b7cdddef60b2e4f3f129dd8f2537c64a271f2"
africastalking.initialize(username, api_key)

sms = africastalking.SMS

ALLOWED_EXTENSIONS = {
    ".pdf", ".PDF", '.txt', '.png', '.jpg', '.jpeg', '.gif', ".png", ".jpg", ".jpeg",
    ".docx"
}
MAX_CONTENT_LENGTH = 10 * 1024 * 1024
applicant = Blueprint("applicant", __name__)
certificates = UploadSet("certificates", ['pdf'])


@applicant.route("/")
@login_required
@check_confirmed
def dashboard():
    """Admin dashboard page."""

    cancelled = current_user.applications.filter_by(status="cancelled").count()
    accepted = current_user.applications.filter_by(status="accepted").count()
    successful_applications = (
        current_user.applications.filter_by(status="applied").order_by(Application.createdAt.desc()).all()
    )
    all_applications = (
        current_user.applications.order_by(Application.createdAt.desc()).all()
    )
    all_jobs = Listing.query.filter_by(published=True).order_by(Listing.createdAt.desc()).all()

    return render_template(
        "applicant/index.html",
        cancelled=cancelled,
        accepted=accepted,
        all_applications=all_applications,
        successful_applications=successful_applications,
        all_jobs=all_jobs,
    )


@applicant.route("/new_application", methods=["post", "get"])
@login_required
@check_confirmed
@checks_required
def new_application():
    """Start a new application."""
    form = SelectJob()
    job_title = form.job.data
    if form.validate_on_submit():
        return redirect(url_for("applicant.start_application", job_title=job_title.job_title))
    return render_template("applicant/select_job.html", form=form)


@applicant.route("/start_application/<job_title>", methods=["post", "get"])
@login_required
@check_confirmed
@checks_required
def start_application(job_title):
    """Apply."""
    form = StartAppForm()
    job = Listing.query.filter_by(job_title=job_title).first_or_404()
    application = Application.query.join(User, (current_user.id == Application.user_id)) \
        .filter(Application.job_listing_id == job.id).first()
    if form.validate_on_submit():
        if application:
            if application.status == 'applied':
                flash("You have applied for {} already boss!".format(job_title), "danger")
                return redirect(url_for("applicant.dashboard"))
        else:
            start_application = Application(
                job_listing_id=job.id,
                user_id=current_user.id
            )
            db.session.add(start_application)
            db.session.commit()
        return redirect(url_for("applicant.edit_application", id=job.id))
    return render_template("applicant/start_application.html", job=job, form=form)


@applicant.route("/apply_job/<id>", methods=["post", "get"])
@login_required
@check_confirmed
@checks_required
def edit_application(id):
    user = current_user
    job_applied = Listing.query.filter_by(id=id).first()
    application_to_complete = Application.query.join(User, (current_user.id == Application.user_id)) \
        .filter(Application.job_listing_id == id).first_or_404()
    this_ed = EducationDocuments.query.filter_by(user_id=user.id).first()
    form = ApplicationDocuments()
    if form.validate_on_submit():
        application_to_complete.agreement = form.terms_and_conditions.data
        docs = EducationDocuments(

            user_id=current_user.id,
            application_id=application_to_complete.id,
            years_of_experience=form.years_of_experience.data,

        )
        days = DaysAvailable(
            application_id=application_to_complete.id,
            monday=form.monday.data,
            tuesday=form.tuesday.data,
            wednesday=form.wednesday.data,
            thursday=form.thursday.data,
            friday=form.friday.data,
            saturday=form.saturday.data,
            sunday=form.sunday.data,
        )
        db.session.add(docs)
        db.session.add(days)
        db.session.commit()
        return redirect(url_for('applicant.submit_application', id=application_to_complete.id))
    return render_template("applicant/apply.html", job_applied=job_applied,
                           application_to_complete=application_to_complete, form=form)


@applicant.route("/start_edit_application/<id>", methods=["post", "get"])
@login_required
@check_confirmed
@checks_required
def start_edit_application(id):
    """Apply."""
    form = EditStartAppForm()
    job = Listing.query.filter_by(id=id).first_or_404()
    application = Application.query.join(User, (current_user.id == Application.user_id)) \
        .filter(Application.job_listing_id == job.id).first()
    if form.validate_on_submit():
        return redirect(url_for("applicant.edit_job_application", id=application.id))
    return render_template("applicant/start_edit_application.html", job=job, form=form)


@applicant.route("/edit_application/<id>", methods=["post", "get"])
@login_required
@check_confirmed
@checks_required
def edit_job_application(id):
    """Edit Application page."""
    application_to_edit = Application.query.filter_by(id=id).first_or_404()
    this_ed = EducationDocuments.query.filter_by(user_id=current_user.id, application_id=id).first()
    job_id = application_to_edit.job_listing_id
    job_applied = Listing.query.filter_by(id=job_id).first()
    form = EditApplicationDocuments(obj=this_ed)
    if form.validate_on_submit():
        application_to_edit.status = 'applied'
        if this_ed:
            form.populate_obj(this_ed)
        else:
            new_docs = EducationDocuments(

                user_id=current_user.id,
                application_id=application_to_edit.id,
                years_of_experience=form.years_of_experience.data

            )
            db.session.add(new_docs)
        db.session.commit()
        flash("Successfully updated.", "success")
        return redirect(url_for('applicant.submit_application', id=application_to_edit.id))
    return render_template("applicant/edit_job_app.html",
                           application_to_edit=application_to_edit, form=form, this_ed=this_ed,
                           job_applied=job_applied)


@applicant.route("/submit_final/<id>", methods=["post", "get"])
@login_required
@check_confirmed
@checks_required
def submit_application(id):
    this_ed = EducationDocuments.query.filter_by(user_id=current_user.id).first()
    this_applicant = Applicant.query.filter_by(user_id=current_user.id).first()
    user = User.query.filter_by(id=current_user.id).first_or_404()
    final_application = Application.query.filter_by(id=id).first()
    final_application.complete_profile = True
    final_application.status = "applied"
    if this_applicant is None:
        applicant = Applicant(
            user_id=current_user.id
        )
        db.session.add(applicant)
        db.session.commit()
    send_applicant_submitted(user)
    flash("Successfully applied for this vacancy.", "success")
    return redirect(url_for("applicant.view_applications"))


@applicant.route("/personal_history", methods=["post", "get"])
@login_required
@check_confirmed
def personal_history():
    """Personal History page."""
    form = LocumPersonalDetailsForm(obj=current_user)
    if form.validate_on_submit():
        form.populate_obj(current_user)
        db.session.commit()
        flash("Personal Details updated successfully", "success")
        return redirect(url_for("applicant.employment_history"))
    return render_template("applicant/personal_details.html", form=form)


@applicant.route("/check_personal_history")
@login_required
@check_confirmed
def check_personal_history():
    if current_user.first_name is not None and current_user.surname is not None and \
            current_user.second_name is not None and current_user.gender is not None and \
            current_user.home_county is not None and current_user.dob is not None and current_user.civil_status is not None and \
            current_user.nationality is not None and current_user.disability is not None and current_user.ethnicity is not None:
        return jsonify({"status": True})
    return jsonify({"status": False})


@applicant.route("/contact_info", methods=["post", "get"])
@login_required
@check_confirmed
def contact_info():
    """Contact Info page."""
    form = ContactInfoForm(obj=current_user)
    if form.validate_on_submit():
        form.populate_obj(current_user)
        db.session.commit()
        flash("Contact Details updated successfully", "success")
        return redirect(url_for("applicant.education_info"))
    return render_template("applicant/contact_info.html", form=form)


@applicant.route("/check_contact_info")
@login_required
@check_confirmed
def check_contact_info():
    if current_user.email is not None and current_user.address is not None and \
            current_user.phone_number_personal is not None and current_user.area_of_residence is not None:
        return jsonify({"status": True})
    return jsonify({"status": False})


@applicant.route("/education_information", methods=["post", "get"])
@login_required
@check_confirmed
def education_info():
    """Education Details page."""
    user = User.query.filter_by(id=current_user.id).first()
    this_ed = LocumAcademicDetails.query.filter_by(user_id=current_user.id).all()
    print(user)
    form = LocumAcademicDetailsForm(obj=user)
    if form.validate_on_submit():
        form.populate_obj(user)
        db.session.commit()
        flash("Academic Details updated successfully.", "success")
        return redirect(url_for("applicant.employment_history_by_details"))
    return render_template("applicant/education_info.html", form=form, this_ed=this_ed)


@applicant.route("/check_education_info")
@login_required
@check_confirmed
def check_education_info():
    ei = LocumAcademicDetails.query.filter_by(user=current_user).first()

    if ei is not None:
        return jsonify({"status": True})
    return jsonify({"status": False})


@applicant.route("/employment_information", methods=["post", "get"])
@login_required
@check_confirmed
def employment_history():
    """Employment History page."""
    this_eh = LocumEmployment.query.filter_by(user_id=current_user.id).first()
    form2 = LocumEmpForm(obj=this_eh)
    if form2.validate_on_submit():
        if this_eh:
            form2.populate_obj(this_eh)
        else:
            eh = LocumEmployment(
                user_id=current_user.id,
                if_not_employed=form2.if_not_employed.data,
                full_time_employed=form2.full_time_employed.data,
                full_time_employer=form2.full_time_employer.data,
                full_time_county=form2.full_time_county.data,
                locum_employed=form2.locum_employed.data,
                locum_employer=form2.locum_employer.data,
                locum_county=form2.locum_employed_county.data,
                terms_of_service=form2.terms_of_service.data,
                position_held=form2.position_held.data,
            )
            db.session.add(eh)
        db.session.commit()
        flash("Employment History updated successfully. Fill in Year by Year Details if applicable.", "success")
        return redirect(url_for("applicant.locum_availability"))
    return render_template("applicant/employment_history.html", form2=form2, this_eh=this_eh)


@applicant.route("/check_employment_history")
@login_required
@check_confirmed
def check_employment_history():
    employment = LocumEmployment.query.filter_by(user=current_user).first()
    if employment is not None:
        return jsonify({"status": True})
    return jsonify({"status": False})


@applicant.route("/_employment_history", methods=["post", "get"])
@login_required
@check_confirmed
def employment_history_by_details():
    user = User.query.filter_by(id=current_user.id).first()
    form = LocumEmploymentForm(obj=user)
    if form.validate_on_submit():
        form.populate_obj(user)
        db.session.commit()
        flash("Employment History updated successfully.", "success")
        return redirect(url_for("applicant.area_of_interest"))
    return render_template("applicant/_employment_history.html", form=form)


@applicant.route("/check_employment_history_by_details")
@login_required
@check_confirmed
def check_employment_history_by_details():
    employment_history = LocumEmploymentHistory.query.filter_by(user=current_user).first()
    if employment_history is not None:
        return jsonify({"status": True})
    return jsonify({"status": False})


@applicant.route("/locum_availability", methods=["post", "get"])
@login_required
@check_confirmed
def locum_availability():
    """Locum page."""
    this_la = LocumAvailability.query.filter_by(user_id=current_user.id).first()
    form = LocumAvailabilityForm(obj=this_la)
    if form.validate_on_submit():
        if this_la:
            form.populate_obj(this_la)
        else:
            la_det = LocumAvailability(
                user_id=current_user.id,
                days_per_month=form.days_per_month.data,
                soonest_date_available=form.soonest_date_available.data,
                affecting_conditions=form.affecting_conditions.data,
                reasons=form.reasons.data
            )
            db.session.add(la_det)
        db.session.commit()
        flash("Details updated successfully", "success")
        return redirect(url_for("applicant.other_personal_details"))
    return render_template("applicant/locum_availability.html", form=form, this_la=this_la)


@applicant.route("/check_locum_availability")
@login_required
@check_confirmed
def check_locum_availability():
    la_info = LocumAvailability.query.filter_by(user=current_user).first()
    if la_info is not None:
        return jsonify({"status": True})
    return jsonify({"status": False})


@applicant.route("/other_personal_details", methods=["post", "get"])
@login_required
@check_confirmed
def other_personal_details():
    """Locum page."""
    this_op = LocumOtherPersonal.query.filter_by(user_id=current_user.id).first()
    form = LocumOtherPersonalDetailsForm(obj=this_op)
    if form.validate_on_submit():
        if this_op:
            form.populate_obj(this_op)
        else:
            op_det = LocumOtherPersonal(
                user_id=current_user.id,
                if_ever_convicted=form.if_ever_convicted.data,
                nature_of_offence=form.nature_of_offence.data,
                year_of_conviction=form.year_of_conviction.data,
                if_ever_dismissed=form.if_ever_dismissed.data,
                reason_for_dismissal=form.reason_for_dismissal.data,
                effective_date=form.effective_date.data
            )
            db.session.add(op_det)
        db.session.commit()
        flash("Details updated successfully", "success")
        return redirect(url_for("applicant.education_info"))
    return render_template("applicant/other_personals.html", form=form, this_op=this_op)


@applicant.route("/check_other_personals")
@login_required
@check_confirmed
def check_other_personals():
    op_info = LocumOtherPersonal.query.filter_by(user=current_user).first()
    if op_info is not None:
        return jsonify({"status": True})
    return jsonify({"status": False})


@applicant.route("/family_details", methods=["post", "get"])
@login_required
@check_confirmed
def family_details():
    """Family Details page."""
    this_family = Family.query.filter_by(user_id=current_user.id).first()
    form = FamilyForm(obj=this_family)
    if form.validate_on_submit():
        if this_family:
            form.populate_obj(this_family)
        else:
            family_det = Family(
                user_id=current_user.id,
                name=form.name.data,
                gender=form.gender.data,
                relationship=form.relationship.data,
                phone_number=form.phone_number.data
            )
            db.session.add(family_det)
        db.session.commit()
        flash("Family Details updated successfully", "success")
        return redirect(url_for("applicant.reference_details"))
    # if this_family:
    #     family_dob = this_family.dob.strftime('%d/%m/%Y')
    return render_template("applicant/family_details.html", form=form, this_family=this_family)


@applicant.route("/check_family_details")
@login_required
@check_confirmed
def check_family_details():
    family_info = Family.query.filter_by(user=current_user).first()
    if family_info is not None:
        return jsonify({"status": True})
    return jsonify({"status": False})


@applicant.route("/area_of_interest", methods=["post", "get"])
@login_required
@check_confirmed
def area_of_interest():
    """Area of interest page."""
    this_roi = LocumAreaOfProfessionalInterest.query.filter_by(user_id=current_user.id).first()
    form = ProfessionalInterestForm(obj=this_roi)
    if form.validate_on_submit():
        if this_roi:
            form.populate_obj(this_roi)
        else:
            roi_det = LocumAreaOfProfessionalInterest(
                user_id=current_user.id,
                name_of_interest=form.name_of_interest.data
            )
            db.session.add(roi_det)
        db.session.commit()
        flash("Details updated successfully", "success")
        return redirect(url_for("applicant.reference_details"))
    return render_template("applicant/area_of_interest.html", form=form, this_roi=this_roi)


@applicant.route("/check_area_of_interest")
@login_required
@check_confirmed
def check_area_of_interest():
    roi_info = LocumAreaOfProfessionalInterest.query.filter_by(user=current_user).first()
    if roi_info is not None:
        return jsonify({"status": True})
    return jsonify({"status": False})


@applicant.route("/references", methods=["post", "get"])
@login_required
@check_confirmed
def reference_details():
    """References page."""
    this_ref = Reference.query.filter_by(user_id=current_user.id).first()
    form = LocumRefereesForm(obj=this_ref)
    if form.validate_on_submit():
        if this_ref:
            form.populate_obj(this_ref)
        else:
            ref_det = Reference(
                user_id=current_user.id,
                name=form.name.data,
                address=form.address.data,
                phone_number=form.phone_number.data,
                email_address=form.email_address.data,
                occupation=form.occupation.data,
                institution=form.institution.data,
                name_2=form.name_2.data,
                address_2=form.address_2.data,
                phone_number_2=form.phone_number_2.data,
                email_address_2=form.email_address_2.data,
                occupation_2=form.occupation_2.data,
                institution_2=form.institution_2.data,
            )
            db.session.add(ref_det)
        db.session.commit()
        flash("Reference Details updated successfully", "success")
        return redirect(url_for("applicant.profile_summary"))
    return render_template("applicant/reference_details.html", form=form)


@applicant.route("/check_reference_details")
@login_required
@check_confirmed
def check_reference_details():
    references = Reference.query.filter_by(user=current_user).first()
    if references is not None:
        return jsonify({"status": True})
    return jsonify({"status": False})


@applicant.route("/view/application/<status>")
@login_required
@check_confirmed
def view_applications_by(status):
    """Admin dashboard page."""
    my_applications = Application.query.filter_by(
        user=current_user, status=status
    ).all()
    return render_template(
        "applicant/application_by_status.html",
        my_applications=my_applications,
        status=status,
    )


@applicant.route("/view/applications")
@login_required
@check_confirmed
def view_applications():
    """Admin dashboard page."""
    user = current_user
    my_applications = Application.query.filter_by(
        user=current_user
    ).order_by(Application.createdAt.desc()).all()
    my_docs = EducationDocuments

    return render_template(
        "applicant/applications.html", my_applications=my_applications, my_docs=my_docs, user=user
    )


@applicant.route("/accept_offer")
@login_required
@check_confirmed
def accept_offer():
    """Admin dashboard page."""
    return redirect(url_for("applicant.dashboard"))


@applicant.route("/decline_offer")
def decline_offer():
    """Admin dashboard page."""
    return render_template("applicant/index.html")


@applicant.route("/profile", methods=["post", "get"])
@login_required
@check_confirmed
def profile():
    """Admin dashboard page."""

    form = ProfileForm(obj=current_user)

    if form.validate_on_submit():
        form.populate_obj(current_user)
        db.session.commit()
        return redirect(url_for("applicant.profile"))
    return render_template("applicant/user-profile-page.html", form=form)


@applicant.route("/profile_summary", methods=["post", "get"])
@login_required
@check_confirmed
def profile_summary():
    """Profile Summary."""
    user = current_user
    education_details = LocumAcademicDetails.query.filter_by(user_id=current_user.id).all()
    employment_details = LocumEmployment.query.filter_by(user_id=current_user.id).first_or_404()
    employment_history = LocumEmploymentHistory.query.filter_by(user_id=current_user.id).all()
    this_la = LocumAvailability.query.filter_by(user_id=current_user.id).all()
    this_op = LocumOtherPersonal.query.filter_by(user_id=current_user.id).all()
    this_roi = LocumAreaOfProfessionalInterest.query.filter_by(user_id=current_user.id).first()

    referees = Reference.query.filter_by(user_id=current_user.id).first_or_404()
    prof = Professional.query.filter_by(user_id=current_user.id).all()
    return render_template("applicant/profile_summary.html", education_details=education_details,
                           employment_history=employment_history, employment_details=employment_details,
                           referees=referees, user=user, prof=prof, this_la=this_la, this_op=this_op,
                           this_roi=this_roi)


@applicant.route("/settings", methods=["post", "get"])
@login_required
@check_confirmed
def settings():
    """Admin dashboard page."""

    if current_user.is_anonymous:
        return redirect(url_for("home.index"))
    form = ChangePasswordForm()
    if form.validate_on_submit():
        if current_user.verify_password(form.old_password.data):
            current_user.password = form.new_password.data
            db.session.add(current_user)
            db.session.commit()
            flash("Your password has been updated.", "success")
            return redirect(url_for("applicant.dashboard"))
        else:
            flash("Original password is invalid.", "warning")
    return render_template("account/reset_password.html", form=form)


@applicant.route("/send_message/<id>", methods=["GET", "POST"])
@login_required
@check_confirmed
def send_message(id):
    user = User.query.filter_by(id=id).first_or_404()
    form = MessageForm()
    if form.validate_on_submit():
        msg = Message(author=current_user, recipient=user, body=form.message.data)
        db.session.add(msg)
        user.add_notification("unread_message_count", user.new_messages())
        db.session.commit()
        flash("Your message has been sent.", "success")
        return redirect(url_for("publisher.messages"))
    return render_template(
        "publisher/send_message.html", title="Send Message", form=form, user=user
    )


@applicant.route("/messages")
@login_required
@check_confirmed
def messages():
    current_user.last_message_read_time = datetime.utcnow()
    current_user.add_notification("unread_message_count", 0)
    db.session.commit()
    page = request.args.get("page", 0, type=int)
    messages = current_user.messages_received.order_by(
        Message.timestamp.desc()
    ).group_by(Message.sender_id)
    return render_template("applicant/messages.html", messages=messages)


@applicant.route("/da_files/<path:filename>")
def uploaded_files(filename):
    path = current_app.config["UPLOAD_FOLDER"]
    return send_from_directory(path, filename)


def allowed_file(filename):
    return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS


@applicant.route("/upload_cv/<app_id>", methods=['GET', 'POST'])
def upload_cv(app_id):
    dirname = 'app/static/uploads/' + str(current_user.id_number)
    if request.method == 'POST':
        uploaded_file = request.files['file']
        filename = secure_filename(uploaded_file.filename)
        our_file = str(current_user.id) + str(app_id) + 'cv' + str(current_user.id_number) + os.path.splitext(filename)[1]
        if filename != '':
            file_ext = os.path.splitext(filename)[1]
            if file_ext not in ALLOWED_EXTENSIONS:
                flash("Application unsuccessful!", "danger")
                flash("CV Needs to be in allowed format!", "danger")
                return "Invalid file", 400
                # Create target directory & all intermediate directories if don't exists
            if not os.path.exists(dirname):
                os.makedirs(dirname)
                print("Directory ", dirname, " Created ")
            else:
                print("Directory ", dirname, " already exists")
            uploaded_file.save(os.path.join(dirname, our_file))
            new_jazz = Jazz(
                user_id=current_user.id,
                application_id=app_id,
                cv_url=our_file
            )
            db.session.add(new_jazz)
            db.session.commit()
        return '', 204


@applicant.route("/upload_license/<app_id>", methods=['GET', 'POST'])
def upload_license(app_id):
    dirname = 'app/static/uploads/' + str(current_user.id_number)
    if request.method == 'POST':
        uploaded_file = request.files['file']
        filename = secure_filename(uploaded_file.filename)
        our_file = str(current_user.id) + str(app_id) + 'l' + str(current_user.id_number) + os.path.splitext(filename)[1]
        if filename != '':
            file_ext = os.path.splitext(filename)[1]
            if file_ext not in ALLOWED_EXTENSIONS:
                flash("Application unsuccessful!", "danger")
                flash("License Needs to be in allowed format!", "danger")
                return "Invalid file", 400
            if not os.path.exists(dirname):
                os.makedirs(dirname)
                print("Directory ", dirname, " Created ")
            else:
                print("Directory ", dirname, " already exists")
            uploaded_file.save(os.path.join(dirname, our_file))
            new_license = License(
                user_id=current_user.id,
                application_id=app_id,
                license=our_file
            )
            db.session.add(new_license)
            db.session.commit()
        return '', 204


def allowed_filesize(filesize):
    if int(filesize) <= MAX_CONTENT_LENGTH:
        return True
    else:
        return False


def gen_file_name(filename):
    """
    If file was exist already, rename it and return a new name
    """

    i = 1
    while os.path.exists(os.path.join(current_app.config["UPLOAD_FOLDER"], filename)):
        name, extension = os.path.splitext(filename)
        filename = "%s_%s%s" % (name, str(i), extension)
        i += 1

    return filename


def create_thumbnail(image):
    try:
        base_width = 100
        img = Image.open(os.path.join(current_app.config["UPLOAD_FOLDER"], image))
        w_percent = base_width / float(img.size[0])
        h_size = int((float(img.size[1]) * float(w_percent)))
        img = img.resize((base_width, h_size), PIL.Image.ANTIALIAS)
        img.save(os.path.join(current_app.config["THUMBNAIL_FOLDER"], image))

        return True

    except:
        print(traceback.format_exc())
        return False


@applicant.route("/upload/<p_table>/<c_table>/<id>", methods=["GET", "POST"])
def upload_image(p_table, c_table, id):
    if request.method == "POST":
        files = request.files["file"]

        if files:
            filename = secure_filename(files.filename)
            filename = gen_file_name(filename)
            mime_type = files.content_type
            p_model = User.get_class_by_tablename(p_table)

            if not allowed_file(files.filename):
                result = uploadfile(
                    name=filename,
                    table=c_table,
                    type=mime_type,
                    size=0,
                    not_allowed_msg="File type not allowed",
                )

            else:
                # save file to disk
                uploaded_file_path = os.path.join(
                    current_app.config["UPLOAD_FOLDER"], filename
                )
                files.save(uploaded_file_path)
                p_model = User.get_class_by_tablename(p_table)
                c_model = User.get_class_by_tablename(c_table)
                parent_table = p_model.query.filter_by(id=id).first_or_404()
                if parent_table.images.count() >= 5:
                    return "You cannot add more than 5 images", 400
                new_image = c_model(image_url=filename, job_listing_id=parent_table.id)
                db.session.add(new_image)
                db.session.commit()

                # create thumbnail after saving
                if mime_type.startswith("image"):
                    create_thumbnail(filename)

                # get file size after saving
                size = os.path.getsize(uploaded_file_path)

                # return json for js call back
                result = uploadfile(
                    name=filename, table=c_table, type=mime_type, size=size
                )

            return simplejson.dumps({"files": [result.get_file()]})

    if request.method == "GET":
        # get all file in ./data directory
        p_model = User.get_class_by_tablename(p_table)
        c_model = User.get_class_by_tablename(c_table)
        parent_table = p_model.query.filter_by(id=id).first_or_404()
        files = [
            f.image_url
            for f in c_model.query.filter_by(job_listing_id=parent_table.id).all()
            if os.path.isfile(
                os.path.join(current_app.config["UPLOAD_FOLDER"], f.image_url)
            )
        ]
        file_display = []

        for f in files:
            size = os.path.getsize(os.path.join(current_app.config["UPLOAD_FOLDER"], f))
            file_saved = uploadfile(name=f, size=size, table=c_table)
            file_display.append(file_saved.get_file())

        return simplejson.dumps({"files": file_display})

    return redirect(url_for("index"))


@applicant.route("/delete_image/<table>/<string:filename>", methods=["DELETE"])
def delete_image(table, filename):
    file_path = os.path.join(current_app.config["UPLOAD_FOLDER"], filename)
    file_thumb_path = os.path.join(current_app.config["THUMBNAIL_FOLDER"], filename)
    c_model = User.get_class_by_tablename(table)
    table = c_model.query.filter_by(image_url=filename).first_or_404()
    db.session.delete(table)
    db.session.commit()
    if os.path.exists(file_path):
        try:
            os.remove(file_path)

            if os.path.exists(file_thumb_path):
                os.remove(file_thumb_path)

            return simplejson.dumps({filename: "True"})
        except:
            return simplejson.dumps({filename: "False"})


# serve static files
@applicant.route("/thumbnail/<string:filename>", methods=["GET"])
def get_thumbnail(filename):
    return send_from_directory(
        current_app.config["THUMBNAIL_FOLDER"], filename=filename
    )


@applicant.route("/data/<string:filename>", methods=["GET"])
def get_file(filename):
    return send_from_directory(
        os.path.join(current_app.config["UPLOAD_FOLDER"]), filename=filename
    )


@applicant.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(current_app.config['UPLOAD_FOLDER'],
                               filename)
