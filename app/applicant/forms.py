from flask import url_for
from flask_wtf import Form, FlaskForm
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms import ValidationError
from wtforms import Form as NoCsrfForm
from wtforms.fields import (
    BooleanField,
    PasswordField,
    StringField,
    SubmitField,
    TextField,
    DateField,
    DecimalField,
    SelectField,
    TextAreaField,
    IntegerField,
    FileField,
    FieldList,
    FormField
)
from wtforms import RadioField
from wtforms.fields.html5 import EmailField
from wtforms.validators import Email, EqualTo, InputRequired, Length, DataRequired, Required, Optional, Regexp
from flask_wtf.file import FileField, FileRequired, FileAllowed
from flask_uploads import UploadSet, configure_uploads, DOCUMENTS, IMAGES, TEXT
from flask_login import current_user
from app.models import User
from app.models import *
from app.models.degree import Degree
from app.models.diploma import Diploma
from app.models.higher_diploma import HigherDiploma
from app.models.masters import Masters
from app.models.phd import Phd
from app.models.certificate import Certificate
from app.models.locum_academic_details import LocumAcademicDetails
from app.models.locum_relevant_course import LocumRelevantCourse
from app.models.locum_employment import LocumEmploymentHistory

certificates = UploadSet("certificates", TEXT + DOCUMENTS + IMAGES)


def get_pk(obj):
    return str(obj)


class ReviewForm(FlaskForm):
    stars = RadioField("Label", coerce=int)
    comment = TextAreaField("Comment", validators=[InputRequired(), Length(1, 300)])
    submit = SubmitField("Submit")


class MessageForm(Form):
    message = TextAreaField(
        "Message", validators=[DataRequired(), Length(min=0, max=140)]
    )
    submit = SubmitField("Submit")


class ProfileForm(FlaskForm):
    first_name = StringField("First name", validators=[InputRequired(), Length(1, 64)])
    surname = StringField("Surname", validators=[InputRequired(), Length(1, 64)])
    email = EmailField("Email", validators=[InputRequired(), Length(1, 64), Email()])
    address = StringField("Address")
    dob = DateField("Date Of Birth", validators=[DataRequired()], format="%d/%m/%Y")

    submit = SubmitField("Submit")


class RequiredIf(object):

    def __init__(self, *args, **kwargs):
        self.conditions = kwargs

    def __call__(self, form, field):
        for name, data in self.conditions.items():
            if name not in form._fields:
                Optional(form, field)
            else:
                condition_field = form._fields.get(name)
                if condition_field.data == data and not field.data:
                    DataRequired()(form, field)
        Optional()(form, field)


class PersonalDetailsForm(FlaskForm):
    title = StringField("Title")
    first_name = StringField("First name", validators=[InputRequired(), Length(1, 64)])
    surname = StringField("Surname", validators=[InputRequired(), Length(1, 64)])
    second_name = StringField(
        "Second name", validators=[InputRequired(), Length(1, 64)]
    )
    gender = SelectField(
        validators=[DataRequired()], choices=[('', ''), ("male", "MALE"), ("female", "FEMALE")], default=''
    )
    test_nationality = SelectField(
        validators=[DataRequired()],
        choices=[('YES', 'YES'), ('NO', 'NO')],
        default='NO'
    )
    home_county = SelectField(
        validators=[RequiredIf(test_nationality == 'YES')],
        choices=[
            ('', ''),
            ("Baringo", "Baringo"),
            ("Bomet", "Bomet"),
            ("Bungoma", "Bungoma"),
            ("Busia", "Busia"),
            ("Diaspora", "Diaspora"),
            ("Elgeyo Marakwet", "Elgeyo Marakwet"),
            ("Embu", "Embu"),
            ("Garissa", "Garissa"),
            ("Homabay", "Homabay"),
            ("Isiolo", "Isiolo"),
            ("Kajiado", "Kajiado"),
            ("Kakamega", "Kakamega"),
            ("Kericho", "Kericho"),
            ("Kiambu", "Kiambu"),
            ("Kilifi", "Kilifi"),
            ("Kirinyaga", "Kirinyaga"),
            ("Kisii", "Kisii"),
            ("Kisumu", "Kisumu"),
            ("Kitui", "Kitui"),
            ("Kwale", "Kwale"),
            ("Laikipia", "Laikipia"),
            ("Lamu", "Lamu"),
            ("Machakos", "Machakos"),
            ("Makueni", "Makueni"),
            ("Mandera", "Mandera"),
            ("Marsabit", "Marsabit"),
            ("Meru", "Meru"),
            ("Migori", "Migori"),
            ("Mombasa", "Mombasa"),
            ("Murang`a", "Murang`a"),
            ("Nairobi", "Nairobi"),
            ("Nakuru", "Nakuru"),
            ("Nandi", "Nandi"),
            ("Narok", "Narok"),
            ("Nyamira", "Nyamira"),
            ("Nyandarua", "Nyandarua"),
            ("Nyeri", "Nyeri"),
            ("Samburu", "Samburu"),
            ("Siaya", "Siaya"),
            ("Taita Taveta", "Taita Taveta"),
            ("Tana River", "Tana River"),
            ("Trans Nzoia", "Trans Nzoia"),
            ("Tharaka Nithi", "Tharaka Nithi"),
            ("Turkana", "Turkana"),
            ("Uasin Gishu", "Uasin Gishu"),
            ("Vihiga", "Vihiga"),
            ("Wajir", "Wajir"),
            ("West Pokot", "West Pokot")
        ],
        default=''
    )

    dob = DateField("Year Of Birth", validators=[DataRequired()], format="%Y")

    civil_status = SelectField(
        validators=[DataRequired()],
        choices=[
            ('', ''),
            ("cohabitation", "Cohabitation"),
            ("divorced", "Divorced"),
            ("domestic partner", "Domestic Partner"),
            ("legally separated", "Legally Separated"),
            ("married", "Married"),
            ("separated", "Separated"),
            ("single", "Single"),
            ("undetermined", "Undetermined"),
        ],
        default=''
    )

    nationality = StringField(
        "Nationality", validators=[RequiredIf(test_nationality == 'NO')]
    )
    ethnicity = StringField("Ethnicity", validators=[RequiredIf(test_nationality == 'YES'), Length(1, 100)])
    country_of_residence = StringField("Your Country", validators=[RequiredIf(test_nationality == 'NO')])
    disability = BooleanField(default=False)
    disability_nature = StringField("Disability Nature", validators=[RequiredIf(disability=True)])
    disability_registration_details = StringField("Disability Registration Details")

    submit = SubmitField("Submit")


class LocumPersonalDetailsForm(FlaskForm):
    title = StringField("Title")
    first_name = StringField("First name", validators=[InputRequired(), Length(1, 64)])
    surname = StringField("Surname", validators=[InputRequired(), Length(1, 64)])
    second_name = StringField(
        "Second name", validators=[InputRequired(), Length(1, 64)]
    )
    gender = SelectField(
        validators=[DataRequired()], choices=[('', ''), ("male", "MALE"), ("female", "FEMALE")], default=''
    )
    test_nationality = SelectField(
        validators=[DataRequired()],
        choices=[('YES', 'YES'), ('NO', 'NO')],
        default='YES'
    )
    home_county = SelectField(
        validators=[RequiredIf(test_nationality == 'YES')],
        choices=[
            ('', ''),
            ("Baringo", "Baringo"),
            ("Bomet", "Bomet"),
            ("Bungoma", "Bungoma"),
            ("Busia", "Busia"),
            ("Diaspora", "Diaspora"),
            ("Elgeyo Marakwet", "Elgeyo Marakwet"),
            ("Embu", "Embu"),
            ("Garissa", "Garissa"),
            ("Homabay", "Homabay"),
            ("Isiolo", "Isiolo"),
            ("Kajiado", "Kajiado"),
            ("Kakamega", "Kakamega"),
            ("Kericho", "Kericho"),
            ("Kiambu", "Kiambu"),
            ("Kilifi", "Kilifi"),
            ("Kirinyaga", "Kirinyaga"),
            ("Kisii", "Kisii"),
            ("Kisumu", "Kisumu"),
            ("Kitui", "Kitui"),
            ("Kwale", "Kwale"),
            ("Laikipia", "Laikipia"),
            ("Lamu", "Lamu"),
            ("Machakos", "Machakos"),
            ("Makueni", "Makueni"),
            ("Mandera", "Mandera"),
            ("Marsabit", "Marsabit"),
            ("Meru", "Meru"),
            ("Migori", "Migori"),
            ("Mombasa", "Mombasa"),
            ("Murang`a", "Murang`a"),
            ("Nairobi", "Nairobi"),
            ("Nakuru", "Nakuru"),
            ("Nandi", "Nandi"),
            ("Narok", "Narok"),
            ("Nyamira", "Nyamira"),
            ("Nyandarua", "Nyandarua"),
            ("Nyeri", "Nyeri"),
            ("Samburu", "Samburu"),
            ("Siaya", "Siaya"),
            ("Taita Taveta", "Taita Taveta"),
            ("Tana River", "Tana River"),
            ("Trans Nzoia", "Trans Nzoia"),
            ("Tharaka Nithi", "Tharaka Nithi"),
            ("Turkana", "Turkana"),
            ("Uasin Gishu", "Uasin Gishu"),
            ("Vihiga", "Vihiga"),
            ("Wajir", "Wajir"),
            ("West Pokot", "West Pokot")
        ],
        default=''
    )

    dob = StringField("Date Of Birth", validators=[DataRequired()])

    civil_status = SelectField(
        validators=[DataRequired()],
        choices=[
            ('', ''),
            ("cohabitation", "Cohabitation"),
            ("divorced", "Divorced"),
            ("domestic partner", "Domestic Partner"),
            ("legally separated", "Legally Separated"),
            ("married", "Married"),
            ("separated", "Separated"),
            ("single", "Single"),
            ("undetermined", "Undetermined"),
        ],
        default=''
    )

    nationality = StringField(
        "Nationality", validators=[RequiredIf(test_nationality == 'NO')]
    )
    ethnicity = StringField("Ethnicity", validators=[RequiredIf(test_nationality == 'YES'), Length(1, 100)])
    country_of_residence = StringField("Your Country", validators=[RequiredIf(test_nationality == 'NO')])
    email = EmailField("Email", validators=[InputRequired(), Length(1, 64), Email()])
    postal_address = StringField("Postal Address", validators=[InputRequired(), Length(1, 64)])
    town = StringField("Address", validators=[InputRequired(), Length(1, 64)])
    phone_number_personal = StringField(
        "Mobile Number", validators=[InputRequired(), Length(1, 64)]
    )
    phone_number_alternate = StringField("Alternate Number")
    disability = BooleanField(default=False)
    disability_nature = StringField("Disability Nature", validators=[RequiredIf(disability=True)])
    disability_registration_details = StringField("Disability Registration Details")

    submit = SubmitField("Submit")


class ContactInfoForm(FlaskForm):
    email = EmailField("Email", validators=[InputRequired(), Length(1, 64), Email()])
    address = StringField("Address", validators=[InputRequired(), Length(1, 64)])
    town = StringField("Address", validators=[InputRequired(), Length(1, 64)])
    phone_number_personal = StringField(
        "Mobile Number", validators=[InputRequired(), Length(1, 64)]
    )
    phone_number_alternate = StringField("Alternate Number")
    area_of_residence = StringField("Area of Residence")
    submit = SubmitField("Save")


class FieldForm(NoCsrfForm):
    year_ended = StringField("Year Ended", validators=[InputRequired(), Length(1, 64)])
    name_of_institution = StringField(
        "Name of Institution", validators=[InputRequired(), Length(1, 64)]
    )
    certificate_obtained = StringField(
        "Certification Obtained", validators=[InputRequired(), Length(1, 64)]
    )


class LocumFieldForm(NoCsrfForm):
    year_from = StringField("Year From", validators=[InputRequired(), Length(1, 64)])
    year_to = StringField("Year Ended", validators=[InputRequired(), Length(1, 64)])
    name_of_institution = StringField(
        "Name of Institution", validators=[InputRequired()]
    )
    award = SelectField(
        choices=[
            ('', ''),
            ("kcpe", "KCPE"),
            ("kcse", "KCSE"),
            ("diploma", "Diploma"),
            ("certificate", "Certificate"),
            ("degree", "Degree"),
            ("masters", "Masters"),
            ("doctorate", "Doctorate"),
        ],
        default=''
    )
    course = StringField("Course", default="")
    specialization = StringField("Specialization", default="")
    class_grade = StringField("Grade Awarded", default="")


class KankaForm(NoCsrfForm):
    year_ended = StringField("Year Ended", default="")
    name_of_certification = StringField(
        "Name of Certification"
    )


class LocumKankaForm(NoCsrfForm):
    year_from = StringField("Year From", default="")
    year_to = StringField("Year Ended", default="")
    name_of_institution = StringField(
        "Name of Institution", default=""
    )
    award = StringField(
        "Award", default=""
    )
    specialization = StringField("Specialization", default="")
    class_grade = StringField("Grade Awarded", default="")


class LocumRelevantCourseForm(NoCsrfForm):
    year_from = StringField("Year From")
    year_to = StringField("Year Ended")
    name_of_institution = StringField(
        "Name of Institution"
    )
    name_of_course = StringField(
        "Name of Course"
    )
    details = StringField("Details")


class BadoForm(NoCsrfForm):
    year_joined = StringField("Year Joined")
    name_of_membership = StringField(
        "Name of Membership"
    )


class LocumBadoForm(NoCsrfForm):
    date_of_renewal = StringField()
    membership_type = StringField(
        "Name of Membership"
    )
    membership_registration_number = StringField(
        "Name of Membership"
    )
    professional_body = StringField(
        "Name of Membership"
    )


class EducationDetailsForm(FlaskForm):
    professional = FieldList(FormField(KankaForm, default=lambda: Professional()), min_entries=1)
    membership = FieldList(FormField(BadoForm, default=lambda: Membership()), min_entries=1)
    highest_level_education = SelectField(
        validators=[DataRequired()],
        choices=[
            ('', ''),
            ("kcpe", "KCPE"),
            ("kcse", "KCSE"),
            ("diploma", "Diploma"),
            ("certificate", "Certificate"),
            ("degree", "Degree"),
            ("masters", "Masters"),
            ("doctorate", "Doctorate"),
        ],
        default=''
    )
    exact_degree_diploma_title = StringField(
        "Exact Degree/Diploma/Certificate", validators=[InputRequired(), Length(1, 64)]
    )
    phd = FieldList(FormField(FieldForm, default=lambda: Phd()), min_entries=1)
    masters = FieldList(FormField(FieldForm, default=lambda: Masters()), min_entries=1)
    degree = FieldList(FormField(FieldForm, default=lambda: Degree()), min_entries=1)
    diploma = FieldList(FormField(FieldForm, default=lambda: Diploma()), min_entries=1)
    higher_diploma = FieldList(FormField(FieldForm, default=lambda: HigherDiploma()), min_entries=1)
    certificate = FieldList(FormField(FieldForm, default=lambda: Certificate()), min_entries=1)
    submit = SubmitField("Save")


class LocumAcademicDetailsForm(FlaskForm):
    professional = FieldList(FormField(LocumKankaForm, default=lambda: Professional()), min_entries=1)
    membership = FieldList(FormField(LocumBadoForm, default=lambda: Membership()), min_entries=1)
    locum_relevant_course = FieldList(FormField(LocumRelevantCourseForm, default=lambda: LocumRelevantCourse()), min_entries=1)

    locum_academic_details = FieldList(FormField(LocumFieldForm, default=lambda: LocumAcademicDetails()), min_entries=1)
    submit = SubmitField("Save")


class EdForm(FlaskForm):
    cv_url = StringField()
    cover_letter_url = StringField()
    application_form_url = StringField()


class ProfessionalForm(FlaskForm):
    professional = FieldList(FormField(KankaForm), min_entries=1)


class MembershipForm(FlaskForm):
    membership = FieldList(FormField(BadoForm), min_entries=1)


class LocumProfessionalForm(FlaskForm):
    professional = FieldList(FormField(LocumKankaForm))


class LocumMembershipForm(FlaskForm):
    membership = FieldList(FormField(LocumBadoForm))


class LocumRelevanceForm(FlaskForm):
    membership = FieldList(FormField(LocumRelevantCourseForm))


class Long(FlaskForm):
    det1 = FormField(EducationDetailsForm)
    det2 = FormField(ProfessionalForm)
    det3 = FormField(MembershipForm)
    det4 = FormField(EdForm)
    submit = SubmitField("Save")


class LocumLong(FlaskForm):
    det1 = FormField(LocumAcademicDetailsForm)
    det2 = FormField(LocumProfessionalForm)
    det3 = FormField(LocumMembershipForm)
    submit = SubmitField("Save")


class TreshForm(NoCsrfForm):
    year_from = StringField(
        "Year From", validators=[InputRequired()]
    )
    year_to = StringField("Year To", validators=[InputRequired()])
    name_of_institution = StringField(
        "Name of Institution", validators=[InputRequired()]
    )
    designation = StringField("designation", validators=[InputRequired()])
    gross_salary = StringField("Gross Salary", validators=[InputRequired()])


class EmploymentForm(FlaskForm):
    employment_history = FieldList(
        FormField(TreshForm, default=lambda: EmploymentHistory()), min_entries=1
    )
    submit = SubmitField("Save")


class LocumEmploymentForm(FlaskForm):
    locum_employment_history = FieldList(
        FormField(TreshForm, default=lambda: LocumEmploymentHistory()), min_entries=1
    )
    submit = SubmitField("Save")


class EmpForm(FlaskForm):
    if_not_employed = BooleanField(default=False)
    ministry = StringField("Ministry")
    station = StringField("Station")
    pf_number = StringField("PF Number")
    personal_employment_no = StringField("Personal/Employment Number")
    present_substantive_post = StringField("Present Substantive Post")
    job_group = StringField("Job Group")
    date_of_current_appointment = DateField("Date of Current Appointment", format="%d/%m/%Y")
    terms_of_service = SelectField(
        choices=[("", ""), ("permanent & pensionable", "PERMANENT & PENSIONABLE"),
                 ("contract", "CONTRACT"), ("other", "OTHER")], default=""
    )
    if_other = TextAreaField("If Other, specify")
    gross_salary = StringField("Gross Salary")
    current_employer = StringField("Current Employer")
    effective_date = DateField("Effective Date", format="%d/%m/%Y")
    position_held = StringField("Position Held")
    submit = SubmitField("Save")


class LocumEmpForm(FlaskForm):
    if_not_employed = BooleanField(default=False)
    full_time_employed = SelectField(
        validators=[DataRequired()],
        choices=[('YES', 'YES'), ('NO', 'NO')],
        default='NO'
    )
    full_time_employer = StringField("Name of Employer")
    full_time_county = SelectField(
        choices=[
            ('', ''),
            ("Baringo", "Baringo"),
            ("Bomet", "Bomet"),
            ("Bungoma", "Bungoma"),
            ("Busia", "Busia"),
            ("Diaspora", "Diaspora"),
            ("Elgeyo Marakwet", "Elgeyo Marakwet"),
            ("Embu", "Embu"),
            ("Garissa", "Garissa"),
            ("Homabay", "Homabay"),
            ("Isiolo", "Isiolo"),
            ("Kajiado", "Kajiado"),
            ("Kakamega", "Kakamega"),
            ("Kericho", "Kericho"),
            ("Kiambu", "Kiambu"),
            ("Kilifi", "Kilifi"),
            ("Kirinyaga", "Kirinyaga"),
            ("Kisii", "Kisii"),
            ("Kisumu", "Kisumu"),
            ("Kitui", "Kitui"),
            ("Kwale", "Kwale"),
            ("Laikipia", "Laikipia"),
            ("Lamu", "Lamu"),
            ("Machakos", "Machakos"),
            ("Makueni", "Makueni"),
            ("Mandera", "Mandera"),
            ("Marsabit", "Marsabit"),
            ("Meru", "Meru"),
            ("Migori", "Migori"),
            ("Mombasa", "Mombasa"),
            ("Murang`a", "Murang`a"),
            ("Nairobi", "Nairobi"),
            ("Nakuru", "Nakuru"),
            ("Nandi", "Nandi"),
            ("Narok", "Narok"),
            ("Nyamira", "Nyamira"),
            ("Nyandarua", "Nyandarua"),
            ("Nyeri", "Nyeri"),
            ("Samburu", "Samburu"),
            ("Siaya", "Siaya"),
            ("Taita Taveta", "Taita Taveta"),
            ("Tana River", "Tana River"),
            ("Trans Nzoia", "Trans Nzoia"),
            ("Tharaka Nithi", "Tharaka Nithi"),
            ("Turkana", "Turkana"),
            ("Uasin Gishu", "Uasin Gishu"),
            ("Vihiga", "Vihiga"),
            ("Wajir", "Wajir"),
            ("West Pokot", "West Pokot")
        ],
        default=''
    )
    locum_employed = SelectField(
        validators=[DataRequired()],
        choices=[('YES', 'YES'), ('NO', 'NO')],
        default='NO'
    )
    locum_employer = StringField("Name of Employer")
    locum_county = SelectField(
        choices=[
            ('', ''),
            ("Baringo", "Baringo"),
            ("Bomet", "Bomet"),
            ("Bungoma", "Bungoma"),
            ("Busia", "Busia"),
            ("Diaspora", "Diaspora"),
            ("Elgeyo Marakwet", "Elgeyo Marakwet"),
            ("Embu", "Embu"),
            ("Garissa", "Garissa"),
            ("Homabay", "Homabay"),
            ("Isiolo", "Isiolo"),
            ("Kajiado", "Kajiado"),
            ("Kakamega", "Kakamega"),
            ("Kericho", "Kericho"),
            ("Kiambu", "Kiambu"),
            ("Kilifi", "Kilifi"),
            ("Kirinyaga", "Kirinyaga"),
            ("Kisii", "Kisii"),
            ("Kisumu", "Kisumu"),
            ("Kitui", "Kitui"),
            ("Kwale", "Kwale"),
            ("Laikipia", "Laikipia"),
            ("Lamu", "Lamu"),
            ("Machakos", "Machakos"),
            ("Makueni", "Makueni"),
            ("Mandera", "Mandera"),
            ("Marsabit", "Marsabit"),
            ("Meru", "Meru"),
            ("Migori", "Migori"),
            ("Mombasa", "Mombasa"),
            ("Murang`a", "Murang`a"),
            ("Nairobi", "Nairobi"),
            ("Nakuru", "Nakuru"),
            ("Nandi", "Nandi"),
            ("Narok", "Narok"),
            ("Nyamira", "Nyamira"),
            ("Nyandarua", "Nyandarua"),
            ("Nyeri", "Nyeri"),
            ("Samburu", "Samburu"),
            ("Siaya", "Siaya"),
            ("Taita Taveta", "Taita Taveta"),
            ("Tana River", "Tana River"),
            ("Trans Nzoia", "Trans Nzoia"),
            ("Tharaka Nithi", "Tharaka Nithi"),
            ("Turkana", "Turkana"),
            ("Uasin Gishu", "Uasin Gishu"),
            ("Vihiga", "Vihiga"),
            ("Wajir", "Wajir"),
            ("West Pokot", "West Pokot")
        ],
        default=''
    )
    date_of_current_appointment = DateField("Date of Current Appointment", format="%d/%m/%Y")
    terms_of_service = StringField("Position Held")
    position_held = StringField("Position Held")
    submit = SubmitField("Save")


class LocumAvailabilityForm(FlaskForm):
    days_per_month = IntegerField("Days (1-30)")
    soonest_date_available = StringField("Date")
    affecting_conditions = SelectField(
        validators=[DataRequired()],
        choices=[('YES', 'YES'), ('NO', 'NO')],
        default='NO'
    )
    reasons = TextAreaField("Reasons", validators=[RequiredIf(affecting_conditions == 'YES')])

    submit = SubmitField("Save")


class LocumOtherPersonalDetailsForm(FlaskForm):
    if_ever_convicted = SelectField(
        validators=[DataRequired()],
        choices=[('YES', 'YES'), ('NO', 'NO')],
        default='NO'
    )
    nature_of_offence = TextAreaField("nature_of_offence", default='Never Convicted')
    year_of_conviction = DateField("Year", format="%Y")
    if_ever_dismissed = SelectField(
        validators=[DataRequired()],
        choices=[('YES', 'YES'), ('NO', 'NO')],
        default='NO'
    )
    reason_for_dismissal = StringField("Reason", default='Never Dismissed')
    effective_date = StringField("Date", default='None')

    submit = SubmitField("Save")


class ProfessionalInterestForm(FlaskForm):
    name_of_interest = StringField("Name")

    submit = SubmitField("Save")


class FamilyForm(FlaskForm):
    name = StringField("Name", validators=[InputRequired(), Length(1, 64)])
    gender = SelectField(
        validators=[DataRequired()], choices=[('', ''), ("male", "MALE"), ("female", "FEMALE")], default=''
    )
    relationship = StringField(
        "Relationship", validators=[InputRequired(), Length(1, 64)]
    )
    phone_number = StringField(
        "Phone Number", validators=[InputRequired(), Length(1, 64)]
    )

    submit = SubmitField("Save")


class ElForm(FlaskForm):
    name = StringField("Name", validators=[InputRequired(), Length(1, 64)])
    address = StringField("Address", validators=[InputRequired(), Length(1, 64)])
    phone_number = StringField(
        "Mobile Number", validators=[InputRequired(), Length(1, 64)]
    )
    email_address = StringField("Email", validators=[InputRequired(), Length(1, 64)])
    occupation = StringField("Occupation", validators=[InputRequired(), Length(1, 64)])
    institution = StringField("Occupation", validators=[InputRequired(), Length(1, 64)])
    name_2 = StringField("Name", validators=[InputRequired(), Length(1, 64)])
    address_2 = StringField("Address", validators=[InputRequired(), Length(1, 64)])
    phone_number_2 = StringField(
        "Mobile Number", validators=[InputRequired(), Length(1, 64)]
    )
    email_address_2 = StringField("Email", validators=[InputRequired(), Length(1, 64)])
    occupation_2 = StringField("Occupation", validators=[InputRequired(), Length(1, 64)])
    institution_2 = StringField("Occupation", validators=[InputRequired(), Length(1, 64)])
    name_3 = StringField("Name", validators=[InputRequired(), Length(1, 64)])
    address_3 = StringField("Address", validators=[InputRequired(), Length(1, 64)])
    phone_number_3 = StringField(
        "Mobile Number", validators=[InputRequired(), Length(1, 64)]
    )
    email_address_3 = StringField("Email", validators=[InputRequired(), Length(1, 64)])
    occupation_3 = StringField("Occupation", validators=[InputRequired(), Length(1, 64)])
    institution_3 = StringField("Occupation", validators=[InputRequired(), Length(1, 64)])
    submit = SubmitField("Save")


class LocumRefereesForm(FlaskForm):
    name = StringField("Name", validators=[InputRequired(), Length(1, 64)])
    address = StringField("Address")
    phone_number = StringField(
        "Mobile Number", validators=[InputRequired(), Length(1, 64)]
    )
    email_address = StringField("Email")
    occupation = StringField("Occupation", validators=[InputRequired(), Length(1, 64)])
    institution = StringField("Occupation")
    name_2 = StringField("Name", validators=[InputRequired(), Length(1, 64)])
    address_2 = StringField("Address")
    phone_number_2 = StringField(
        "Mobile Number", validators=[InputRequired(), Length(1, 64)]
    )
    email_address_2 = StringField("Email")
    occupation_2 = StringField("Occupation", validators=[InputRequired(), Length(1, 64)])
    institution_2 = StringField("Occupation")

    submit = SubmitField("Save")


class SelectJob(FlaskForm):
    job = QuerySelectField(
        "Select Job",
        validators=[InputRequired()],
        get_label="job_title",
        query_factory=lambda: db.session.query(Listing).order_by("job_title").filter_by(published=True),
        get_pk=get_pk,
    )
    user_id = IntegerField("User ID")
    submit = SubmitField("Select Job")


class StartAppForm(FlaskForm):
    submit = SubmitField("APPLY JOB")


class EditStartAppForm(FlaskForm):
    submit = SubmitField("EDIT JOB APPLICATION")


class ApplicationForm(FlaskForm):
    job_listing_id = IntegerField("Job ID")
    user_id = IntegerField("User ID")
    complete_profile = StringField("Complete Profile")

    submit = SubmitField("APPLY")


def validate_file_extension(form, field):
    import os
    ext = os.path.splitext(field.data)[1]  # [0] returns path+filename
    valid_extensions = ['.pdf', '.PDF', '.txt', '.png', '.jpg', '.jpeg', '.gif', '.docx']
    if not ext.lower() in valid_extensions:
        raise ValidationError('Unsupported file extension. Try uploading PDF Documents!')


def my_strip_filter(value):
    if value is not None and hasattr(value, 'strip'):
        value = value.replace("  ", " ").replace(" ", "_").replace("(", "").replace(")", "")
        return value
    return value


class MyBaseForm(FlaskForm):
    class Meta:
        def bind_field(self, form, unbound_field, options):
            filters = unbound_field.kwargs.get('filters', [])
            filters.append(my_strip_filter)
            return unbound_field.bind(form=form, filters=filters, **options)


class ApplicationDocuments(MyBaseForm):
    cv_url = StringField()
    cover_letter_url = StringField()
    application_form_url = StringField()
    license = StringField()
    terms_and_conditions = BooleanField(validators=[DataRequired()], default=False)
    monday = BooleanField(default=False)
    tuesday = BooleanField(default=False)
    wednesday = BooleanField(default=False)
    thursday = BooleanField(default=False)
    friday = BooleanField(default=False)
    saturday = BooleanField(default=False)
    sunday = BooleanField(default=False)
    application_id = IntegerField()
    years_of_experience = StringField(validators=[DataRequired()])

    submit = SubmitField("SUBMIT MY APPLICATION TO THE ABOVE VACANCY")


class EditApplicationDocuments(MyBaseForm):
    cv_url = StringField()
    cover_letter_url = StringField()
    application_form_url = StringField()
    license = StringField()
    terms_and_conditions = BooleanField(validators=[DataRequired()], default=False)
    application_id = IntegerField()
    years_of_experience = IntegerField(validators=[DataRequired()])

    submit = SubmitField("EDIT")
