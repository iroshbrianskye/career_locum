var selector = document.getElementsByClassName("gs");
var year = document.getElementsByClassName("year");

var im2 = new Inputmask("9999");
im2.mask(year);

$(function() {
    'use strict';

    $('#datePicker').datepicker({
          format: "yyyy",
          viewMode: "years",
          minViewMode: "years",
          maxViewMode: "decades",
          autoclose: true
    });

    $('#datePicker2').datepicker({
          format: "yyyy",
          viewMode: "years",
          minViewMode: "years",
          maxViewMode: "decades",
          autoclose: true
    });

});