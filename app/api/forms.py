from flask import url_for
from flask_wtf import Form, FlaskForm
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms import ValidationError
from wtforms import Form as NoCsrfForm
from wtforms.fields import (
    BooleanField,
    PasswordField,
    StringField,
    SubmitField,
    TextField,
    DateField,
    DecimalField,
    SelectField,
    TextAreaField,
    IntegerField,
    FileField,
    FieldList,
    FormField, FloatField
)
from wtforms import RadioField
from wtforms.fields.html5 import EmailField
from wtforms.validators import Email, EqualTo, InputRequired, Length, DataRequired, Required, Optional, Regexp
from flask_wtf.file import FileField, FileRequired, FileAllowed
from flask_uploads import UploadSet, configure_uploads, DOCUMENTS, IMAGES, TEXT
from flask_login import current_user
from app.models import User
from app import db
from app.models import Listing, EmploymentHistory

certificates = UploadSet("certificates", TEXT + DOCUMENTS + IMAGES)


def get_pk(obj):
    return str(obj)


class MessageForm(Form):
    message = TextAreaField(
        "Message", validators=[DataRequired(), Length(min=0, max=140)]
    )
    submit = SubmitField("Submit")


class ProfileForm(FlaskForm):
    first_name = StringField("First name", validators=[InputRequired(), Length(1, 64)])
    surname = StringField("Surname", validators=[InputRequired(), Length(1, 64)])
    email = EmailField("Email", validators=[InputRequired(), Length(1, 64), Email()])
    address = StringField("Address")
    dob = DateField("Date Of Birth", validators=[DataRequired()], format="%d/%m/%Y")

    submit = SubmitField("Submit")


class RequiredIf(object):

    def __init__(self, *args, **kwargs):
        self.conditions = kwargs

    def __call__(self, form, field):
        for name, data in self.conditions.items():
            if name not in form._fields:
                Optional(form, field)
            else:
                condition_field = form._fields.get(name)
                if condition_field.data == data and not field.data:
                    DataRequired()(form, field)
        Optional()(form, field)


class TreshForm(NoCsrfForm):
    year_from = StringField(
        "Year From", validators=[InputRequired()]
    )
    year_to = StringField("Year To", validators=[InputRequired()])
    name_of_institution = StringField(
        "Name of Institution", validators=[InputRequired()]
    )
    designation = StringField("designation", validators=[InputRequired()])
    gross_salary = StringField("Gross Salary", validators=[InputRequired()])


class DataEntryForm(FlaskForm):
    job_title = SelectField(validators=[InputRequired()], choices=[], coerce=int)
    title = SelectField(
        validators=[DataRequired()],
        choices=[
            ('', ''),
            ("Mr.", "Mr."),
            ("Mrs.", "Mrs."),
            ("Ms.", "Ms."),
            ("Dr.", "Dr."),
            ("Prof.", "Prof.")
        ],
        default=''
    )
    job_reference_number = StringField("")
    job_listing_id = IntegerField()
    id_number = StringField("ID Number")
    first_name = StringField("First name")
    surname = StringField("Surname")
    second_name = StringField(
        "Second name"
    )
    gender = SelectField(
        validators=[DataRequired()], choices=[('', ''), ("male", "MALE"), ("female", "FEMALE")], default=''
    )
    test_nationality = SelectField(
        validators=[DataRequired()],
        choices=[('YES', 'YES'), ('NO', 'NO')],
        default='NO'
    )
    home_county = SelectField(
        validators=[RequiredIf(test_nationality == 'YES')],
        choices=[
            ('', ''),
            ("Baringo", "Baringo"),
            ("Bomet", "Bomet"),
            ("Bungoma", "Bungoma"),
            ("Busia", "Busia"),
            ("Diaspora", "Diaspora"),
            ("Elgeyo Marakwet", "Elgeyo Marakwet"),
            ("Embu", "Embu"),
            ("Garissa", "Garissa"),
            ("Homabay", "Homabay"),
            ("Isiolo", "Isiolo"),
            ("Kajiado", "Kajiado"),
            ("Kakamega", "Kakamega"),
            ("Kericho", "Kericho"),
            ("Kiambu", "Kiambu"),
            ("Kilifi", "Kilifi"),
            ("Kirinyaga", "Kirinyaga"),
            ("Kisii", "Kisii"),
            ("Kisumu", "Kisumu"),
            ("Kitui", "Kitui"),
            ("Kwale", "Kwale"),
            ("Laikipia", "Laikipia"),
            ("Lamu", "Lamu"),
            ("Machakos", "Machakos"),
            ("Makueni", "Makueni"),
            ("Mandera", "Mandera"),
            ("Marsabit", "Marsabit"),
            ("Meru", "Meru"),
            ("Migori", "Migori"),
            ("Mombasa", "Mombasa"),
            ("Murang`a", "Murang`a"),
            ("Nairobi", "Nairobi"),
            ("Nakuru", "Nakuru"),
            ("Nandi", "Nandi"),
            ("Narok", "Narok"),
            ("Nyamira", "Nyamira"),
            ("Nyandarua", "Nyandarua"),
            ("Nyeri", "Nyeri"),
            ("Samburu", "Samburu"),
            ("Siaya", "Siaya"),
            ("Taita Taveta", "Taita Taveta"),
            ("Tana River", "Tana River"),
            ("Trans Nzoia", "Trans Nzoia"),
            ("Tharaka Nithi", "Tharaka Nithi"),
            ("Turkana", "Turkana"),
            ("Uasin Gishu", "Uasin Gishu"),
            ("Vihiga", "Vihiga"),
            ("Wajir", "Wajir"),
            ("West Pokot", "West Pokot")
        ],
        default=''
    )
    age = StringField("Age")
    civil_status = SelectField(
        validators=[DataRequired()],
        choices=[
            ('', ''),
            ("cohabitation", "Cohabitation"),
            ("divorced", "Divorced"),
            ("domestic partner", "Domestic Partner"),
            ("legally separated", "Legally Separated"),
            ("married", "Married"),
            ("separated", "Separated"),
            ("single", "Single"),
            ("undetermined", "Undetermined"),
        ],
        default=''
    )
    nationality = StringField(
        "Nationality", validators=[RequiredIf(test_nationality == 'NO')]
    )
    ethnicity = StringField("Ethnicity", validators=[RequiredIf(test_nationality == 'YES'), Length(1, 100)])
    country_of_residence = StringField("Your Country", validators=[RequiredIf(test_nationality == 'NO')])
    phone_number_personal = StringField(
        "Mobile Number", validators=[InputRequired(), Length(1, 64)]
    )
    email = EmailField("Email", validators=[InputRequired(), Length(1, 64), Email()])
    address = StringField("Address", validators=[InputRequired(), Length(1, 64)])
    phone_number_alternate = StringField("Alternate Number")
    disability = BooleanField(default=False)
    disability_nature = StringField("Disability Nature", validators=[RequiredIf(disability=True)])
    disability_registration_details = StringField("Disability Registration Details")
    highest_level_education = SelectField(
        validators=[DataRequired()],
        choices=[
            ('', ''),
            ("kcpe", "KCPE"),
            ("kcse", "KCSE"),
            ("diploma", "Diploma"),
            ("certificate", "Certificate"),
            ("degree", "Degree"),
            ("masters", "Masters"),
            ("doctorate", "Doctorate"),
        ],
        default=''
    )
    phd_certificate_obtained = TextAreaField(
        "Certification Obtained"
    )
    masters_certificate_obtained = TextAreaField(
        "Certification Obtained"
    )
    degree_certificate_obtained = TextAreaField(
        "Certification Obtained"
    )
    diploma_certificate_obtained = TextAreaField(
        "Certification Obtained"
    )
    higher_diploma_certificate_obtained = TextAreaField(
        "Certification Obtained"
    )
    certificate_obtained = TextAreaField(
        "Certification Obtained"
    )
    relevant_certification = TextAreaField(
        "Name of Certification"
    )
    registration = TextAreaField(
        "Registration To A Body"
    )
    total_work_experience = FloatField(
        "Total work experience", validators=[InputRequired()]
    )
    relevant_work_experience = FloatField(
        "Relevant work experience", validators=[InputRequired()]
    )
    mode_of_application = SelectField(
        validators=[DataRequired()],
        choices=[('', ''), ("post-office", "POST OFFICE"), ("hand-delivery", "HAND DELIVERY")], default=''
    )
    data_entered_by = IntegerField(
        "Entered by"
    )
    cv = BooleanField(default=False)
    cover_letter = BooleanField(default=False)
    application_form = BooleanField(default=False)

    submit = SubmitField("Submit")


class EditDataEntryForm(FlaskForm):
    job_title = SelectField(validators=[InputRequired()], choices=[])
    title = SelectField(
        validators=[DataRequired()],
        choices=[
            ('', ''),
            ("Mr.", "Mr."),
            ("Mrs.", "Mrs."),
            ("Ms.", "Ms."),
            ("Dr.", "Dr."),
            ("Prof.", "Prof.")
        ],
        default=''
    )
    job_reference_number = StringField("")
    job_listing_id = IntegerField()
    id_number = StringField("ID Number")
    first_name = StringField("First name")
    surname = StringField("Surname")
    second_name = StringField(
        "Second name"
    )
    gender = SelectField(
        validators=[DataRequired()], choices=[('', ''), ("male", "MALE"), ("female", "FEMALE")], default=''
    )
    test_nationality = SelectField(
        validators=[DataRequired()],
        choices=[('YES', 'YES'), ('NO', 'NO')],
        default='NO'
    )
    home_county = SelectField(
        validators=[RequiredIf(test_nationality == 'YES')],
        choices=[
            ('', ''),
            ("Baringo", "Baringo"),
            ("Bomet", "Bomet"),
            ("Bungoma", "Bungoma"),
            ("Busia", "Busia"),
            ("Diaspora", "Diaspora"),
            ("Elgeyo Marakwet", "Elgeyo Marakwet"),
            ("Embu", "Embu"),
            ("Garissa", "Garissa"),
            ("Homabay", "Homabay"),
            ("Isiolo", "Isiolo"),
            ("Kajiado", "Kajiado"),
            ("Kakamega", "Kakamega"),
            ("Kericho", "Kericho"),
            ("Kiambu", "Kiambu"),
            ("Kilifi", "Kilifi"),
            ("Kirinyaga", "Kirinyaga"),
            ("Kisii", "Kisii"),
            ("Kisumu", "Kisumu"),
            ("Kitui", "Kitui"),
            ("Kwale", "Kwale"),
            ("Laikipia", "Laikipia"),
            ("Lamu", "Lamu"),
            ("Machakos", "Machakos"),
            ("Makueni", "Makueni"),
            ("Mandera", "Mandera"),
            ("Marsabit", "Marsabit"),
            ("Meru", "Meru"),
            ("Migori", "Migori"),
            ("Mombasa", "Mombasa"),
            ("Murang`a", "Murang`a"),
            ("Nairobi", "Nairobi"),
            ("Nakuru", "Nakuru"),
            ("Nandi", "Nandi"),
            ("Narok", "Narok"),
            ("Nyamira", "Nyamira"),
            ("Nyandarua", "Nyandarua"),
            ("Nyeri", "Nyeri"),
            ("Samburu", "Samburu"),
            ("Siaya", "Siaya"),
            ("Taita Taveta", "Taita Taveta"),
            ("Tana River", "Tana River"),
            ("Trans Nzoia", "Trans Nzoia"),
            ("Tharaka Nithi", "Tharaka Nithi"),
            ("Turkana", "Turkana"),
            ("Uasin Gishu", "Uasin Gishu"),
            ("Vihiga", "Vihiga"),
            ("Wajir", "Wajir"),
            ("West Pokot", "West Pokot")
        ],
        default=''
    )
    age = StringField("Age")
    civil_status = SelectField(
        validators=[DataRequired()],
        choices=[
            ('', ''),
            ("cohabitation", "Cohabitation"),
            ("divorced", "Divorced"),
            ("domestic partner", "Domestic Partner"),
            ("legally separated", "Legally Separated"),
            ("married", "Married"),
            ("separated", "Separated"),
            ("single", "Single"),
            ("undetermined", "Undetermined"),
        ],
        default=''
    )
    nationality = StringField(
        "Nationality", validators=[RequiredIf(test_nationality == 'NO')]
    )
    ethnicity = StringField("Ethnicity", validators=[RequiredIf(test_nationality == 'YES'), Length(1, 100)])
    country_of_residence = StringField("Your Country", validators=[RequiredIf(test_nationality == 'NO')])
    phone_number_personal = StringField(
        "Mobile Number", validators=[InputRequired(), Length(1, 64)]
    )
    email = EmailField("Email", validators=[InputRequired(), Length(1, 64), Email()])
    address = StringField("Address", validators=[InputRequired(), Length(1, 64)])
    phone_number_alternate = StringField("Alternate Number")
    disability = BooleanField(default=False)
    disability_nature = StringField("Disability Nature", validators=[RequiredIf(disability=True)])
    disability_registration_details = StringField("Disability Registration Details")
    highest_level_education = SelectField(
        validators=[DataRequired()],
        choices=[
            ('', ''),
            ("kcpe", "KCPE"),
            ("kcse", "KCSE"),
            ("diploma", "Diploma"),
            ("certificate", "Certificate"),
            ("degree", "Degree"),
            ("masters", "Masters"),
            ("doctorate", "Doctorate"),
        ],
        default=''
    )
    phd_certificate_obtained = TextAreaField(
        "Certification Obtained"
    )
    masters_certificate_obtained = TextAreaField(
        "Certification Obtained"
    )
    degree_certificate_obtained = TextAreaField(
        "Certification Obtained"
    )
    diploma_certificate_obtained = TextAreaField(
        "Certification Obtained"
    )
    higher_diploma_certificate_obtained = TextAreaField(
        "Certification Obtained"
    )
    certificate_obtained = TextAreaField(
        "Certification Obtained"
    )
    relevant_certification = TextAreaField(
        "Name of Certification"
    )
    registration = TextAreaField(
        "Registration To A Body"
    )
    total_work_experience = FloatField(
        "Total work experience", validators=[InputRequired()]
    )
    relevant_work_experience = FloatField(
        "Relevant work experience", validators=[InputRequired()]
    )
    mode_of_application = SelectField(
        validators=[DataRequired()],
        choices=[('', ''), ("post-office", "POST OFFICE"), ("hand-delivery", "HAND DELIVERY")], default=''
    )
    data_entered_by = IntegerField(
        "Entered by"
    )
    cv = BooleanField(default=False)
    cover_letter = BooleanField(default=False)
    application_form = BooleanField(default=False)

    submit = SubmitField("Submit")


class EmpForm(FlaskForm):
    employment_history = FieldList(
        FormField(TreshForm, default=lambda: EmploymentHistory()), min_entries=1
    )

    submit = SubmitField("Submit")
