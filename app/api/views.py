import uuid

from flask import (
    Blueprint,
    render_template,
    redirect,
    url_for,
    jsonify,
    flash,
    send_from_directory)
from flask_ckeditor import upload_success, upload_fail
from flask_login import current_user, login_required, login_user
import PIL, simplejson, traceback
from PIL import Image
from flask_rq import get_queue
from flask_uploads import patch_request_class

from app.models.job_listings import *
from app.models.applications import *
from app.models.education_documents import *
from app.models.users import *
from app.api.forms import *
from app.auth.forms import *
from app import db
from app.email import send_email
from app.models import *
from app.auth.email import send_password_reset_email
from app.auth.admin_decorators import check_confirmed
from app.decorators import checks_required
from werkzeug.utils import secure_filename
from app.lib.upload_file import uploadfile
from app.auth.email import send_applicant_submitted

publisher = Blueprint("publisher", __name__)


@publisher.route("/")
@login_required
@check_confirmed
def dashboard():
    app_count = ManualEntry.query.filter_by(data_entered_by=current_user.id).count()
    manual_applications = ManualEntry.query.filter_by(data_entered_by=current_user.id) \
        .order_by(ManualEntry.createdAt.desc()).all()
    return render_template("publishers/dashboard.html", app_count=app_count, manual_applications=manual_applications)


@publisher.route("/applications")
@login_required
@check_confirmed
def applications():
    manual_applications = ManualEntry.query.order_by(ManualEntry.createdAt.desc()).all()
    return render_template("publishers/applications.html", manual_applications=manual_applications)


@publisher.route("/add_application", methods=("GET", "POST"))
@login_required
@check_confirmed
def add_applicant():
    form = DataEntryForm()
    form.job_title.choices = [(row.id, row.job_title) for row in Listing.query.all()]
    if form.validate_on_submit():
        listing = Listing.query.filter_by(id=form.job_title.data).first_or_404()
        manual_user = User(
            first_name=form.first_name.data,
            second_name=form.second_name.data,
            surname=form.surname.data,
            gender=form.gender.data,
            id_number=form.id_number.data,
            email=form.email.data,
            phone_number_personal=form.phone_number_personal.data,
            address=form.address.data,
            country_of_residence=form.country_of_residence.data,
            home_county=form.home_county.data,
            ethnicity=form.ethnicity.data,
            phone_number_alternate=form.phone_number_alternate.data,
            civil_status=form.civil_status.data,
            nationality=form.nationality.data,
            disability=form.disability.data,
            disability_nature=form.disability_nature.data,
            disability_registration_details=form.disability_registration_details.data,
            highest_level_education=form.highest_level_education.data,
            role_id='2'
        )
        me = ManualEntry(
            job_title=listing.job_title,
            job_reference_number=listing.job_reference_number,
            job_listing_id=listing.id,
            title=form.title.data,
            first_name=form.first_name.data,
            second_name=form.second_name.data,
            surname=form.surname.data,
            gender=form.gender.data,
            id_number=form.id_number.data,
            email=form.email.data,
            phone_number_personal=form.phone_number_personal.data,
            address=form.address.data,
            age=form.age.data,
            country_of_residence=form.country_of_residence.data,
            home_county=form.home_county.data,
            ethnicity=form.ethnicity.data,
            phone_number_alternate=form.phone_number_alternate.data,
            civil_status=form.civil_status.data,
            nationality=form.nationality.data,
            disability=form.disability.data,
            disability_nature=form.disability_nature.data,
            disability_registration_details=form.disability_registration_details.data,
            highest_level_education=form.highest_level_education.data,
            phd_certificate_obtained=form.phd_certificate_obtained.data,
            masters_certificate_obtained=form.masters_certificate_obtained.data,
            degree_certificate_obtained=form.degree_certificate_obtained.data,
            diploma_certificate_obtained=form.diploma_certificate_obtained.data,
            higher_diploma_certificate_obtained=form.higher_diploma_certificate_obtained.data,
            certificate_obtained=form.certificate_obtained.data,
            relevant_certification=form.relevant_certification.data,
            registration=form.registration.data,
            total_work_experience=form.total_work_experience.data,
            relevant_work_experience=form.relevant_work_experience.data,
            mode_of_application=form.mode_of_application.data,
            data_entered_by=current_user.id,
            cv=form.cv.data,
            cover_letter=form.cover_letter.data,
            application_form=form.application_form.data
        )
        db.session.add(manual_user)
        db.session.add(me)
        db.session.commit()
        flash("Manual Application Submitted Successfully.", "success")
        return redirect(url_for("publisher.employment_history_by_details", user_id=manual_user.id))
    return render_template("publishers/add_applicant.html", form=form)


@publisher.route("/_employment_history/<user_id>", methods=["post", "get"])
@login_required
@check_confirmed
def employment_history_by_details(user_id):
    user = User.query.filter_by(id=user_id).first()
    form = EmpForm(obj=user)
    if form.validate_on_submit():
        form.populate_obj(user)
        db.session.commit()
        flash("Employment History updated successfully.", "success")
        return redirect(url_for("publisher.dashboard"))
    return render_template("publishers/_employment_history.html", form=form, user=user)


@publisher.route("/_get_model")
def _get_model():
    job_title = request.args.get("job_title", 0, type=str)
    listing = Listing.query.filter_by(job_title=job_title).first_or_404()
    job_reference = listing.job_reference_number
    return jsonify(job_reference)


@publisher.route("/edit_application/<id>", methods=("GET", "POST"))
@login_required
@check_confirmed
def edit_application(id):
    app = ManualEntry.query.filter_by(id=id).first_or_404()
    form = EditDataEntryForm(obj=app)
    form.job_title.choices = [(row.job_title, row.job_title) for row in Listing.query.all()]
    if form.validate_on_submit():
        form.populate_obj(app)
        manual_user = User(
            first_name=form.first_name.data,
            second_name=form.second_name.data,
            surname=form.surname.data,
            gender=form.gender.data,
            id_number=form.id_number.data,
            email=form.email.data,
            phone_number_personal=form.phone_number_personal.data,
            address=form.address.data,
            country_of_residence=form.country_of_residence.data,
            home_county=form.home_county.data,
            ethnicity=form.ethnicity.data,
            phone_number_alternate=form.phone_number_alternate.data,
            civil_status=form.civil_status.data,
            nationality=form.nationality.data,
            disability=form.disability.data,
            disability_nature=form.disability_nature.data,
            disability_registration_details=form.disability_registration_details.data,
            highest_level_education=form.highest_level_education.data,
            role_id='2'
        )

        db.session.add(manual_user)
        db.session.commit()
        flash("Manual Application Edited Successfully.", "success")
        return redirect(url_for("publisher.employment_history_by_details", user_id=manual_user.id))
    return render_template('publishers/edit_application.html', form=form, app=app)
